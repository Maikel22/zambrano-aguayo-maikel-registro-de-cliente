var correo = document.getElementById("correo");
var formulario = document.getElementById("formulario");

function validarEmail() {
    var noVali= /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
    var valido= noVali.test(correo.value);
    if (valido==true){
        alert("registrado exitosamente")
    }
   else {
     alert("La dirección de email es incorrecta.");
    }
  }
  document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("formulario").addEventListener('submit', validarFormulario); 
  });
  
  function validarFormulario(evento) {
    evento.preventDefault();
    var nombre = document.getElementById('nombres').value;
    if(nombre.length == 0) {
      alert('No has escrito nada en el usuario');
      return;
    }
    var apellidos = document.getElementById('apellidos').value;
    if (apellidos.length < 6) {
      alert('No has escrito el apellido');
      return;
    }
    this.submit();
  }
